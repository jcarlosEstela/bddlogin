//
//  LoginTests.swift
//  BDDLoginPrueba
//
//  Created by José Estela on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import XCTest
import OHHTTPStubs
import GIGLibrary
@testable import BDDLoginPrueba

class LoginTests: XCTestCase {
    
    // MARK: - Attributes
    
    var presenter: LoginPresenter!
    var loginViewMock: LoginViewMock!
    
    // MARK: - Setup methods
    
    override func setUp() {
        super.setUp()
        self.loginViewMock = LoginViewMock()
        self.presenter = LoginPresenter(
            view: self.loginViewMock,
            loginInteractor: LoginInteractor(
                service: LoginService()
            )
        )
    }
    
    override func tearDown() {
        self.loginViewMock = nil
        self.presenter = nil
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    // MARK: - Test methods
    
    func testPresenterNotNil() {
        XCTAssertNotNil(self.presenter)
    }
    
    func test_loginPresenter_showSuccessInView_whenAPIResponseIsSuccess() {
        // ARRANGE
        self.loginViewMock.expectation = self.expectation(description: "api return")
        let _ = stub(condition: isPath("/login"), response: { _ in
            return StubResponse.stubResponse(with: "login_ok.json")
        })
        
        // ACT
        self.presenter.userRequestsLogin(email: "test@gigigo.com", pass: "12345")
        
        // ASSERT
        self.waitForExpectations(timeout: 1.0, handler: nil)
        XCTAssert(self.loginViewMock.spyShow.called == true)
        XCTAssert(self.loginViewMock.spyShow.message == "SUCESS")
    }
    
    func test_loginPresenter_showWrongUserInView_whenAPIResponseFailed() {
        // ARRANGE
        self.loginViewMock.expectation = self.expectation(description: "api return")
        let _ = stub(condition: isPath("/login"), response: { _ in
            return StubResponse.stubResponse(with: "login_ko.json")
        })
        
        // ACT
        self.presenter.userRequestsLogin(email: "test@gigigo.com", pass: "12345")
        
        // ASSERT
        self.waitForExpectations(timeout: 1.0, handler: nil)
        XCTAssert(self.loginViewMock.spyShow.called == true)
        XCTAssert(self.loginViewMock.spyShow.message == "Usuario incorrecto", self.loginViewMock.spyShow.message)
    }
    
    func test_loginPresenter_showEmptyMail_whenMailIsEmpty() {
        // ARRANGE
        
        // ACT
        self.presenter.userRequestsLogin(email: "", pass: "12345")
        
        // ASSERT
        XCTAssert(self.loginViewMock.spyShow.called == true)
        XCTAssert(self.loginViewMock.spyShow.message == "Email vacío", self.loginViewMock.spyShow.message)
    }
    
    func test_loginPresenter_setParamsToAPI() {
        // ARRANGE
        self.loginViewMock.expectation = self.expectation(description: "api return")
        OHHTTPStubs.stubRequests(passingTest: { request -> Bool in
            // Get the request (to check the body)
            let data = (request as NSURLRequest).ohhttpStubs_HTTPBody()
            let json = try! JSON.dataToJson(data!)
            // Assert that email & password are OK
            XCTAssert(json["email"]?.toString() == "test@gigigo.com")
            XCTAssert(json["password"]?.toString() == "12345")
            return true
        }) { _ in
            return StubResponse.stubResponse(with: "login_ok.json")
        }
        
        // ACT
        self.presenter.userRequestsLogin(email: "test@gigigo.com", pass: "12345")
        
        // ASSERT
        self.waitForExpectations(timeout: 1.0, handler: nil)
    }
}
