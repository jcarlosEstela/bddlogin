//
//  LoginViewMock.swift
//  BDDLoginPrueba
//
//  Created by José Estela on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import XCTest
@testable import BDDLoginPrueba

class LoginViewMock: LoginView {
    
    // MARK: - Attributes
    
    var spyShow = (called: false, message: "")
    var expectation: XCTestExpectation?
    
    // MARK: - LoginView
    
    func showMessage(message: String) {
        self.spyShow.called = true
        self.spyShow.message = message
        self.expectation?.fulfill()
    }
}
