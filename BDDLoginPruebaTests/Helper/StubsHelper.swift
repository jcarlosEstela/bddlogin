//
//  StubsHelper.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 14/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import OHHTTPStubs

class StubResponse {
	
	class func stubResponse(with json: String) -> OHHTTPStubsResponse {
		return OHHTTPStubsResponse(
			fileAtPath: OHPathForFile(json, StubResponse.self)!,
			statusCode: 200,
			headers: ["Content-Type":"application/json"]
		)
	}	
}
