//
//  LoginInteractor.swift
//  BDDLoginPrueba
//
//  Created by José Estela on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import GIGLibrary

struct LoginInteractor {
    
    // MARK: - Attributes
    
    let service: LoginService
    
    // MARK: - Input methods
    
    func login(email: String, pass: String, completion: @escaping (Result<Bool, String>) -> Void) {
        guard !email.isEmpty else  {
            completion(.error("Email vacío"))
            return
        }
        self.service.login(email: email, pass: pass) { result in
            switch result {
            case .success(_):
                completion(.success(true))
            case .error(let message):
                completion(.error(message))
            }
        }
    }
}
