//
//  AppDelegate.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 14/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import UIKit
import GIGLibrary


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		guard let loginVC = try? Instantiator<LoginVC>().viewController() else { return false }
        let presenter = LoginPresenter(
            view: loginVC,
            loginInteractor: LoginInteractor(
                service: LoginService()
            )
        )
        loginVC.presenter = presenter
		
		self.window = UIWindow(frame: UIScreen.main.bounds)
		self.window?.rootViewController = loginVC
		self.window?.makeKeyAndVisible()
		
		return true
	}

}

