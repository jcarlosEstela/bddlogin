//
//  LoginVC.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 14/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import UIKit
import GIGLibrary


class LoginVC: UIViewController, KeyboardAdaptable {
	
	// MARK: - UI Properties
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var labelStatus: UILabel!
	
    // MARK: - Attributes
    var presenter: LoginPresenter?
	
	// MARK: - UI Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
		self.initializeView()
    }
	
	override func viewDidDisappear(_ animated: Bool) {
		self.stopKeyboard()
		
		super.viewDidDisappear(animated)
	}
	
	// MARK: - UI Actions
	
	@IBAction func onButtonLoginTap(_ sender: Any) {
        self.presenter?.userRequestsLogin(
            email: self.emailTextField.text!,
            pass: self.passwordTextField.text!
        )
	}
	
	// MARK: - Private Helpers
	
	private func initializeView() {
		self.labelStatus.text = ""
		self.startKeyboard()
	}
	
}

extension LoginVC: Instantiable {
	static func storyboard() -> String { return "Login" }
}

extension LoginVC: LoginView {
    
    func showMessage(message: String) {
        self.labelStatus.text = message
    }
}
