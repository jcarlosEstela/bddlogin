//
//  LoginService.swift
//  BDDLoginPrueba
//
//  Created by José Estela on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import GIGLibrary

struct LoginService {
    
    // MARK: - Input methods
    
    func login(email: String, pass: String, completion: @escaping (Result<Bool, String>) -> Void ) {
        let request = Request(
            method: "POST",
            baseUrl: "https://gigigo.com",
            endpoint: "/login",
            headers: ["Content-Type": "application/json"],
            bodyParams: [
                "email": email,
                "password": pass
            ],
            verbose: true
        )
        request.fetch { response in
            switch response.status {
            case .success:
                completion(.success(true))
            default:
                completion(.error("Usuario incorrecto"))
            }
        }
    }
}
