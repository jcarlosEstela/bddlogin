//
//  LoginPresenter.swift
//  BDDLoginPrueba
//
//  Created by José Estela on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation

protocol LoginView {
    func showMessage(message: String)
}

struct LoginPresenter {
    
    // MARK: - Attributes
    
    let view: LoginView
    let loginInteractor: LoginInteractor
    
    // MARK: - Input methods
    
    func userRequestsLogin(email: String, pass: String) {
        self.loginInteractor.login(email: email, pass: pass) { result in
            switch result {
            case .success(_):
                self.view.showMessage(message: "SUCESS")
            case .error(let message):
                self.view.showMessage(message: message)
            }
        }
    }
}
